#!/bin/bash
function ask_yes_or_no() {
    read -p "$1 ([y]es or [n]o): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes|o|oui) echo "oui" ;;
        *)     echo "non" ;;
    esac
}

echo "Automated installation of Portainer/Traefik stack"
echo ""
echo "To be able to complete this installation, you must have before"
echo "installed Hypriot on your Raspberry-pi (3 or better recommanded)."
echo "You must also have registered an internet domaine name and "
echo "open an account at Cloudflare and transfer your DNS server (FREE) there."
echo ""


if [[ "non" == $(ask_yes_or_no "Did you get your internet domain name and your Cloudflare account info?") ]]
then
    echo "You need to get an internet domain name and a Cloudflare account, then run that script again."
    exit 0
fi

read -p "Host name for Portainer service: " INPUT_PORTAINER_HOSTNAME
read -p "Host name for Traefic service: " INPUT_TRAEFIK_HOSTNAME
read -p "Email for Lets'Encrypt recovery: " INPUT_LETSENCRYPT_EMAIL
read -p "Email for your Cloudflare account: " INPUT_CLOUDFLARE_EMAIL
read -p "Cloudflare Global API Key: " INPUT_CLOUDFLARE_APIKEY
read -p "Domaine name for the Docker network: " INPUT_DOCKER_DOMAIN

docker swarm init
docker network create -d overlay proxied_network

PORTAINER_HOSTNAME=$INPUT_PORTAINER_HOSTNAME \
TRAEFIK_HOSTNAME=$INPUT_TRAEFIK_HOSTNAME \
LETSENCRYPT_EMAIL=$INPUT_LETSENCRYPT_EMAIL \
CLOUDFLARE_EMAIL=$INPUT_CLOUDFLARE_EMAIL \
CLOUDFLARE_API_KEY=$INPUT_CLOUDFLARE_APIKEY \
DOCKER_DOMAIN=$INPUT_DOCKER_DOMAIN \
docker stack deploy -c docker-compose.yml maestro

echo "Installation complete, take a coffee and then navigate to your server URL :"
echo "HTTPS://$PORTAINER_HOSTNAME"
echo "Create you administrative user and then enjoy free hosting your life yourself"
echo ""
echo "Any bugs can be reported here : https://framagit.org/linuq/easy-pi-hosting/issues"
echo "French documentation of the projet is here : https://linuq.org/projets/auto-hebergement_maison"
